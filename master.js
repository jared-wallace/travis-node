const cluster = require('cluster');
const fs = require("fs");
var logger = require("./logger.js");
var config = {};

logger.info('started master with ' + process.pid);

//fork the first process
var worker = cluster.fork();
worker.on("message", (data) => {
  config = data;
});


process.on('SIGHUP', function () {
  logger.info("Saving configuration...");
  fs.writeFile(
    './config.json',
    JSON.stringify(config, null, 2),
    (err) => {
      if (err) {
        logger.error("Unable to save configuration");
      }
      logger.info("Save successful, reloading");
      worker = cluster.fork();
      worker.on("message", (data) => {
        config = data;
      });
      worker.once('listening', function () {
        //stop all other workers
        for(var id in cluster.workers) {
          if (id === worker.id.toString()) continue;
          cluster.workers[id].kill('SIGTERM');
        }
      });
    });
});

process.on('SIGINT', () => {
  logger.info("Received request to shutdown");
  logger.info("Saving configuration...");
  fs.writeFile(
    './config.json',
    JSON.stringify(config, null, 2),
    (err) => {
      if (err) {
        logger.error("Unable to save configuration");
      }
      logger.info("Save successful, exiting");
      process.exit();
    });
});
