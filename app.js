const express = require("express");
const request = require("request");
const bodyParser = require("body-parser");
const fs = require("fs");
const https = require("https");
const util = require("util");
const cluster = require("cluster");
const ww = require("./lib/ww");
const travis = require("./travis");
const WW_URL = "https://api.watsonwork.ibm.com";

var privateKey = fs.readFileSync("key.pem");
var certificate = fs.readFileSync("cert.pem");
var app = express();
var token = {};
var config = require('./config.json');
var logger = require("./logger.js");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

if (cluster.isMaster) {
  return require("./master");
}
process.send(config);

// Callback from Watson Workspace.
app.post("/ww", function (req, res) {
  logger.info("Received POST to /ww");
  var body = req.body;
  var eventType = body.type;

  // Verification event
  if (eventType === "verification") {
    logger.debug("Verifying...");
    ww.verifyWorkspace(res, body.challenge, config.webhook_key);
    return;
  }
  res.status(200).end();
  // Message created event
  if (eventType === "message-created") {
    // Ignore our own messages
    if (body.userId === config.app_id) {
      return;
    }
    var text = body.content.toLowerCase();
    // Handle if we were mentioned
    if (text.includes('travis')) {
      logger.info("We were mentioned in a message");
      logger.debug("Message was: " + text);
      travis.handleMessage(config, body, token);
    }
  }
});

app.post("/callback", function (req, res) {
  logger.info("Received POST to /callback");
  var body = req.body;
});

https.createServer({
  key: privateKey,
  cert: certificate
}, app).listen(config.server_port, function (err, res) {
  logger.info("Server started on port " + config.server_port);
});

// Grab our initial WW auth token. They're good for roughly 12 hours.
ww.getToken(WW_URL, config.appid, config.appsecret, function (err, res) {
  if (err) {
    logger.error("Failed to obtain initial token", err);
  } else {
    token = res;
    // Start our loops
    travis.startLoops(config, token);
  }
});

