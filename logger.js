const winston = require('winston');
const fs = require("fs");
const env = process.env.NODE_ENV || 'development';
const log_directory = "logs";
// Create the log directory if it does not exist
if (!fs.existsSync(log_directory)) {
  fs.mkdirSync(log_directory);
}

const tsFormat = () => (new Date()).toLocaleTimeString();

const logger = module.exports = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      timestamp: tsFormat,
      colorize: true,
      level: 'info',
      prettyPrint: true
    }),
    new (require('winston-daily-rotate-file'))({
      filename: `${log_directory}/-travis.log`,
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd',
      prepend: true,
      level: env === 'development' ? 'silly' : 'info'
    })
  ]
});


exports.info = function() {
    logger.info.apply(this, arguments);
};

exports.warn = function() {
    logger.info.apply(this, arguments);
};

exports.error = function() {
    logger.info.apply(this, arguments);
};

exports.debug = function() {
    logger.info.apply(this, arguments);
};
