const request = require("request");
const crypto = require("crypto");
var logger = require("../logger.js");

// These global variables get set on the initial auth token call. They won't
// change during the lifetime of the program.
var _id = "";
var _secret = "";
var _url = "";

var check_expiration = function (token, callback) {
  logger.debug("Entering ww.check_expiration");
  // Check for token expiration
  now = new Date();
  // If we are within 10 seconds of having our token expire, go ahead and grab a
  // new one.
  if (now.getTime() > token.expires.getTime() - 10000) {
    logger.debug("Token almost expired, getting new one");
    module.exports.getToken(_url, _id, _secret, function (err, res) {
      if (err) {
        logger.error("Unable to renew token. App will no longer post to the space.", err);
        callback(err, null);
      } else {
        token = res;
        logger.info("Renewed token which now expires at " + token.expires.toString());
        callback(null, true);
      }
    });
  } else {
    logger.debug("Token not expired, not renewing");
    callback(null, true);
  }
  logger.debug("Leaving ww.check_expiration");
}

// Send a message to a WW space.
module.exports.sendMessage = function (msg, color, space, token) {
  logger.debug("Entering ww.sendMessage.");
  // Check for token expiration
  check_expiration(token, function(err, res) {
    if (err) {
      logger.error("Unable to check expiration of token");
      return;
    } else {
      const appMessage = {
        "type": "appMessage",
        "version": "1",
        "annotations": [{
          "type": "generic",
          "version": "1",
          "title": "",
          "text": "",
          "color": color,
        }]
      };

      const sendMessageOptions = {
        "url": _url + "/v1/spaces/" + space + "/messages",
        "headers": {
          "Content-Type": "application/json",
          "jwt": token.value
        },
        "method": "POST",
        "body": ""
      };

      appMessage.annotations[0].text = msg;
      try {
        sendMessageOptions.body = JSON.stringify(appMessage);
      }
      catch (e) {
        logger.error("Could not stringify message", e);
      }

      request(sendMessageOptions, function(err, res, sendMessageBody) {
        if (err) {
          logger.error("Could not send message to WW space because", err);
        } else {
          logger.info("Successfully sent message to WW space.");
        }
      });

    }
  });
  logger.debug("Leaving ww.sendMessage.");
};

// Make a call to the WW GraphQL API endpoint.
module.exports.makeGraphQLCall = function (body, token, callback) {
  logger.debug("Entering ww.makeGraphQLCall");
  check_expiration(token, function (err, res) {
    if (err) {
      logger.error("Could not check expiration of token");
      return;
    } else {
      const sendMessageOptions = {
        "url": _url + "/graphql",
        "headers": {
          "Content-Type": "application/graphql",
          "x-graphql-view": "PUBLIC",
          "jwt": token.value},
        "method": "POST",
        "body": body
      };
      request(sendMessageOptions, function(err, res, body) {
        if (err) {
          logger.error("Could not make GraphQL request.", err);
          callback(err, null);
        } else {
          logger.debug("GraphQL call succeeded.");
          try {
            callback(null, JSON.parse(body));
          } catch (err) {
            logger.error("GraphQL result was unreadable", err);
            callback(err, null);
          }
        }
      });
    }
  });
  logger.debug("Leaving ww.makeGraphQLCall");
};

// Respond to a verification request for a WW webhook.
module.exports.verifyWorkspace = function (response, challenge, secret) {
  logger.debug("Entering ww.verifyWorkspace.");
  // Create the object that is going to be sent back to Workspace for
  // verification.
  var bodyChallenge = {
    "response": challenge // This is what we end up hashing
  };

  try {
    var responseBodyString = JSON.stringify(bodyChallenge);
  }
  catch (e) {
    logger.error("Could not stringify verification challenge");
  }

  var tokenForVerification = crypto
    // Use the webhook secret as hash key
    .createHmac("sha256", secret)
    // and hash the body ("response": challenge)
    .update(responseBodyString)
    // ending up with the hex formatted hash.
    .digest("hex");
  // Write our response headers
  response.setHeader("Content-Type", "application/json; charset=utf-8");
  response.setHeader("X-OUTBOUND-TOKEN", tokenForVerification);
  response.writeHead(200);

  // Add our body and send it off.
  response.end(responseBodyString);
  logger.debug("Webhook was verified");
  logger.debug("Leaving ww.verifyWorkspace.");
};

// Obtain a token for posting to a WW space.
module.exports.getToken = function (url, id, secret, callback) {
  logger.debug("Entering ww.getToken");
  _id = id;
  _secret = secret;
  _url = url;
  request({
      url: url + "/oauth/token",
      method: 'POST',
      auth: {
        user: id,
        pass: secret
      },
      form: {
        'grant_type': 'client_credentials'
      }
  }, function(err, res) {
    if (err) {
      logger.error("Failed to get token", err);
      callback(err, null);
    } else {
      var token = {};
      try {
        token.value =  JSON.parse(res.req.res.body).access_token;
        expires = JSON.parse(res.req.res.body).expires_in;
        // expiration is set to "now"
        var expiration = new Date();
        // extract epoch time in milliseconds
        var timeobj = expiration.getTime();
        // add the token expiration time in milliseconds to it
        timeobj += (expires * 1000);
        // Finally, update expiration to reflect actual token expiration
        expiration.setTime(timeobj);
        token.expires = expiration;
        logger.info("Obtained initial token that expires at " + token.expires.toString());
        callback(null, token);
      }
      catch (e) {
        logger.error("Could not parse reply from IBM successfully", err);
        callback(err, null);
      }
    }
  });
  logger.debug("Leaving ww.getToken");
}

module.exports.decodeUTF = function (s) {
  logger.debug("Entered ww.decode_utf8.");
  return unescape(encodeURIComponent(s));
  logger.debug("Leaving ww.decode_utf8.");
}

module.exports.analyzeMessage = function (body, token, callback) {
  /*
   * metadata {
   *   author: "xx",
   *   id: "xx",
   *   message: "xx",
   *   keywords: [],
   *   sentiment: {
   *     type: "xx",
   *     score: xx
   *   }
   */
  logger.debug("Entering ww.analyzeMessage");
  var metadata = {};
  var msg_id = body.messageId;
  var graphql_body = "{ message (id: \"" + msg_id + "\")" +
    "{ createdBy { displayName id }" +
    " created content contentType annotations } }";
  module.exports.makeGraphQLCall(graphql_body, token, function (err, res) {
    if (err) {
      logger.error("Could not analyze message because: ", err);
      callback(err, null);
    }
    logger.debug("GraphQL returned: ", res);

    var data = res.data.message;
    var annotation_length = data.annotations.length;

    // Since every file upload creates a message with the filename,
    // we need to remove that 'garbage' for proper handling. That's
    // what the regex below is doing. The message text looks like
    // "<$xxx123>".
    metadata.message = module.exports.decodeUTF(data.content).replace(/<\$.*>/, "");
    if (metadata.message === "") {
      // somebody uploaded an attachment
      logger.debug("An image or file was posted, ignoring");
      callback(null, null);
    }

    metadata.id = data.createdBy.id;
    metadata.author = data.createdBy.displayName;

    for (var i = 0; i < annotation_length; i++) {
      try {
        var note = JSON.parse(data.annotations[i]);
        if (note.type === "message-nlp-keywords") {
          metadata.keywords = note.keywords;
        } else if (note.type = "message-nlp-docSentiment") {
          metadata.sentiment = {type: note.docSentiment.type,
            score: note.docSentiment.score};
        }
      }
      catch (e) {
        logger.error("Failed to parse annotation ");
      }
    }
    logger.silly("Message analysis returned: ", metadata);
    callback(null, metadata);
  });
}

