const schedule = require("node-schedule");
const ww = require("./lib/ww");
const app = require("./app");
const news = require("./news");
var logger = require("./logger.js");

module.exports.startLoops = function (config, token) {
  logger.debug("Entering travis.startLoops");
  if (config.news.enabled) {
    logger.info("News module is enabled");
    if (config.news.periodic === "yes" && config.news.quantity > 1) {
      logger.info("Hourly news is enabled");
      // Cron timing:
      // minute (0-59), hour(0-23), day of month(1-31), month(1-12), weekday(0-6, Sunday is 6)
      // asterisk means all possible numbers
      // numbers can be multiple, seperated by comma
      // asterisk divided by a number x means every x increments (*/5 * * * * means every 5 minutes)
      schedule.scheduleJob("0 */3 * * *", function () {
        news.spamNews(config, token);
      });
    } else {
      logger.error("Hourly news is enabled, but the quantity of items is invalid.");
      logger.error("The quantity requested was: ", config.news.quantity);
      logger.error("This can be adjusted in the config file.");
      return;
    }
    if (config.news.watch === "yes") {
      var msg = "";
      config.news.keywords.forEach(function (e) {
        msg += e + ", ";
      });
      logger.info("News updates are enabled with the following keywords: " + msg.slice(0,-2));
      news.checkNews(config, token);
      setInterval(news.checkNews, 60000, config, token);
    }
  }
  logger.debug("Leaving travis.startLoops");
}

module.exports.handleMessage = function (config, body, token) {
  ww.analyzeMessage(body, token, function (err, metadata) {
    if (err || !metadata) {
      logger.info("Ignoring message");
      return;
    }
    if (metadata.id === config.appid) {
      logger.info("Ignoring our own jabbering");
      return;
    }
    if (metadata.sentiment.type === "negative" && metadata.sentiment.score < config.threshold) {
      // insult
      logger.info("Somebody's talking trash, time to let them know who we are");
      return;
    }
  });
}

