const parser = require("rss-parser");
const striptags = require("striptags");
const ww = require("./lib/ww");
const moment = require("moment-timezone");
const schedule = require("node-schedule");
var logger = require("./logger.js");


module.exports.spamNews = function (config, token) {
  logger.debug("Entering news.spamNews");
  var name = config.news.hourly_url.name;
  var url = config.news.hourly_url.url;

  parser.parseURL(url, (err, parsed) => {
    msg = "It's " + moment.tz().format("H:m") + " GMT, and here are some of the stories we're following on " + name + ":\n";
    entries = parsed.feed.entries;
    for (var i = 0; i < config.news.quantity; i++) {
      try {
        var title = entries[i].title;
        var link = entries[i].link;
        var snippet = entries[i].contentSnippet;
        var summary = striptags(snippet).replace(/[\t\n]+/g, "");
        msg += "[" + title + "](";
        msg += link + ") - _";
        msg += summary + "_\n";
        logger.info("Added hourly news item");
      } catch (err) {
        logger.error("Failed to add hourly news item", err);
      }
    }
    logger.info("Attempting to send hourly news to space");
    ww.sendMessage(msg, config.news.color, config.spaceid, token);
    logger.debug("Message was: ", msg);
  });
  logger.debug("Leaving news.spamNews");
}

module.exports.checkNews = function (config, token) {
  logger.debug("Entering news.checkNews");
  // Set our "old posts" delete funtion to run every day at midnight
  schedule.scheduleJob("0 0 * * *", () => {
    deletePosts(config);
  });
  // update repo
  updateRepo(config, (err, res) => {
    if (res) {
      logger.debug("Update succeeded");
      // search feeds
      searchFeeds(res, token, (err, results) => {
        // cross-check with older messages sent
        var posts = config.news_repo.posts;
        logger.silly("Posts: ", posts);
        for (source in results) {
          // Do we have old posts from this source?
          if (posts[source]) {
            logger.debug("We have older posts from this source");
            // Then for each news item from this source in results,
            // check against older entries from that source. We want to avoid
            // sending duplicate messages to the space.
            results[source].forEach( (element) => {
              var posts_titles_array = [];
              posts[source].forEach( (item) => {
                posts_titles_array.push(item.title);
              });
              if (posts_titles_array.includes(element.title)) {
                // Remove duplicate entry from results
                logger.debug("Removing duplicate entry");
                logger.silly("Removing entry: ", element.title);
                results[source] = results[source].filter( (e) => {
                  return e.title !== element.title;
                });
              } else {
                logger.silly("Element: " + element.title);
                logger.silly("not equal to any of these: ", posts_titles_array);
              }
            });
          }
        }
        // Check to see if we removed all entries for a given source.
        var toDelete = [];
        for (source in results) {
          if (results[source].length < 1) {
            logger.debug("Removing source '" + source + "' because no entries remain");
            toDelete.push(source);
          }
        }
        toDelete.forEach( (element) => {
          delete results[element];
        });
        // add new posts to the message tracker
        for (source in results) { // for each source
          for (entry in results[source]) { // for each entry of that source
            try {
              // Does posts already have that source?
              logger.debug("Appending entry to existing source");
              posts[source].push(results[source][entry]);
            } catch (e) {
              // If not, create it.
              logger.debug("Creating new source and adding entry to it");
              posts[source] = [results[source][entry]];
            }
          }
        }
        // send message to space with news headlines discovered
        for (source in results) {
          msg = "Just in from " + source + ":\n";
          for (entry in results[source]) {
            var title = results[source][entry].title;
            var link = results[source][entry].link;
            var snippet = results[source][entry].contentSnippet;
            var summary = striptags(snippet).replace(/[\t\n]+/g, "");
            msg += "[" + title + "](";
            msg += link + ") - _";
            msg += summary + "_\n";
            logger.info("Added recent news item");
          }
          ww.sendMessage(msg.slice(0,-1), config.std_color, config.spaceid, token);
        }
      });
    }
  });
}

var deletePosts = function (config) {
  logger.debug("Entering news.deletePosts");
  config.news_repo.posts = {};
  logger.debug("List of old news has been purged");
}

var searchFeeds = function (config, token, callback) {
  logger.debug("Entered news.searchFeeds");
  keywords = config.news.keywords;
  if (!keywords || keywords.length < 1) {
    logger.error("Invalid number of keywords specified.");
    logger.error("Correct this oversight in config.json");
    callback({error: "Invalid number of keywords"}, null);
  } else {
    // results = {
    //   source1: [ entry1, entry2 ],
    //   source2: [ entry1, entry2 ]
    // }
    results = {};
    var feeds = config.news_repo.feeds;
    var keywords = config.news.keywords;
    var feeds_length = Object.keys(feeds).length;
    logger.debug("Feed length is: ", feeds_length);
    var keywords_length = keywords.length;
    for (feed in feeds) {
      logger.debug("Searching feed " + feed);
      logger.silly("feed is: ", feeds[feed]);
      var entries = feeds[feed];
      var entries_length = entries.length;
      logger.debug("Entries length is: ", entries_length);
      for (var j = 0; j < entries_length; j++) {
        for (var k = 0; k < keywords_length; k++) {
          try {
            if (entries[j].title.toLowerCase().split(' ').indexOf(keywords[k]) > 0) {
              // We found a winner
              logger.debug("Adding a matching feed entry to results array");
              logger.silly("Entry added is: ", entries[j]);
              try {
                results[feed].push(entries[j]);
              } catch (e) {
                results[feed] = [entries[j]];
              }
              logger.silly("Results looks like", results);
            } else {
              logger.silly("No match in title");
            }
          } catch (e) {
            logger.error("Couldn't filter entry by keyword: ", e);
            logger.error("Entry in question was: ", entries[j]);
            logger.error("Feed in question was: ", feed);
          }
        }
      }
    }
    logger.silly("Results: ", results);
    callback(null, results);
  }
}

var updateRepo = function (config, callback) {
  // news_repo = {
  //   feeds: {
  //     source1: rss_feed},
  //     source2: rss_feed2}
  //   },
  //   posts: {
  //     source1: [ entry1, entry2 ],
  //     source2: [ entry1, entry2 ]
  //   }
  // }
  logger.debug("Entered updateRepo");
  logger.info("Pulling fresh news stories");
  if (!config.news_repo) {
    logger.debug("Creating news_repo");
    config.news_repo = {};
    config.news_repo.feeds = {};
    config.news_repo.posts = {};
  }
  var urls = config.news.monitoring_urls;
  for (var i = 0; i < urls.length; i++) {
    var url = urls[i].url;
    parseURL(url, i, config).then(result => {
      var entries = result.entries;
      var name = result.name;
      var url = result.url;
      logger.silly("Feed name is " + name);
      logger.silly("Feed url is: " + url);
      config.news_repo.feeds[name] = entries;
      logger.info("Updated feed " + name);
    })
    .catch(err => {
      logger.error("Had an issue parsing the RSS feed: ", err);
    });
  }
  logger.silly("feeds now looks like: ", config.news_repo.feeds);
  process.send(config);
  callback(null, config);
}

var parseURL = function (url, i, config) {
  logger.debug("Entering parseURL");
  logger.silly("Parsing: " + url);
  return new Promise((resolve, reject ) => {
    parser.parseURL(url, (err, parsed) => {
      if (err) {
        return reject(err);
      }
      var urls = config.news.monitoring_urls;
      var name = urls[i].name;
      result = {};
      result.name = name;
      result.url = url;
      result.entries = parsed.feed.entries;
      return resolve(result);
    });
  });
}

